package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class FriendsTabs extends Sprite 
	{
		
		private static var _starlingInstance:Starling;
		private static var _flashVars:Object;
		
		public static function get starlingInstance():Starling {
			return _starlingInstance;
		}
		public static function get flashVars():Object {
			return _flashVars;
		}
		
		private static var _src:String;
		public static function get src():String {
			return _src;
		}
		
		
		public function FriendsTabs() {
			_flashVars = stage.loaderInfo.parameters as Object;
			
			//_src = "graphics/sprite_sheet"; //local setting
			_src = "https://kondrakov.github.io/sprite_sheet"; //remote setting
			
			_starlingInstance = new Starling(Game, stage);
			_starlingInstance.start();
		}
		
	}
	
}