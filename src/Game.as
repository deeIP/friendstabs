package 
{
	import starling.display.Sprite;
	import starling.events.Event;
	
	import robotlegs.bender.framework.api.IContext;
	import robotlegs.bender.framework.impl.Context;
	import robotlegs.extensions.starlingViewMap.StarlingViewMapExtension;
	import robotlegs.bender.extensions.mediatorMap.MediatorMapExtension;
	import robotlegs.bender.extensions.localEventMap.LocalEventMapExtension;
	import robotlegs.bender.extensions.eventDispatcher.EventDispatcherExtension;
	import utils.extensions.actor.ActorExtension;
	import robotlegs.bender.extensions.signalCommandMap.SignalCommandMapExtension;
	
	import application.config.ApplicationConfig;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class Game extends Sprite {
		
		private var _context:IContext;
		
		public function Game() {
			_context = new Context();
			
			_context.install(
				EventDispatcherExtension,
				SignalCommandMapExtension,
				LocalEventMapExtension,
				MediatorMapExtension,
				StarlingViewMapExtension,
				ActorExtension
			);
			_context.configure(
				ApplicationConfig,
				FriendsTabs.starlingInstance
			);
			_context.initialize();
		}
	}

}