package application.config
{
	import starling.core.Starling;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;
	import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.bundles.mvcs.Command;
	
	import application.view.ApplicationView;
	import application.view.ApplicationMediator;
	
	import application.model.ConfigDataProvider;
	import application.model.VKAPIService;
	import application.model.ISocialProxy;
	
	import application.view.IBitmapRequestView;
	import application.view.BitmapRequestMediator;
	import application.view.IAtlasRequestView;
	import application.view.AtlasRequestMediator;
	
	
	import application.view.userProfile.PickFriendsCompositionMediator;
	import application.view.userProfile.PickFriendsCompositionView;
	import application.view.userProfile.UserProfileListMediator;
	import application.view.userProfile.UserProfileListView;
	import application.view.userProfile.UserProfileMediator;
	import application.view.userProfile.UserProfileView;

	import application.view.alert.AlertView;
	import application.view.alert.AlertMeidiator;
	import application.notifications.NtfRequestError;
	import application.notifications.NtfOverlayPanelClosed;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class ApplicationConfig implements IConfig {

		[Inject]
		public var injector:IInjector;
		
		[Inject]
		public var starling:Starling;
		
		[Inject]
		public var commandMap:ISignalCommandMap;
		
		[Inject]
		public var mediatorMap:IMediatorMap;
		
		public function ApplicationConfig() {
			
		}

		public function configure():void {
			
			injector.map(ConfigDataProvider).asSingleton();
			injector.map(VKAPIService).asSingleton(true);
			
			injector.map(NtfRequestError).asSingleton();
			injector.map(NtfOverlayPanelClosed).asSingleton();
			
			//TODO Можно сделать выбор маппинга класса в зависимости от соцсети
			injector.map(ISocialProxy).toSingleton(VKAPIService);
			
			mediatorMap.map(ApplicationView).toMediator(ApplicationMediator);
			mediatorMap.map(IBitmapRequestView).toMediator(BitmapRequestMediator);
			mediatorMap.map(IAtlasRequestView).toMediator(AtlasRequestMediator);
			mediatorMap.map(AlertView).toMediator(AlertMeidiator);
			
			mediatorMap.map(PickFriendsCompositionView).toMediator(PickFriendsCompositionMediator);
			mediatorMap.map(UserProfileListView).toMediator(UserProfileListMediator);
			mediatorMap.map(UserProfileView).toMediator(UserProfileMediator);

			var applicationView:ApplicationView = new ApplicationView();
			injector.injectInto(applicationView);
			starling.stage.addChild(applicationView);
			injector.map(ApplicationView).toValue(applicationView);
		}

	}

}