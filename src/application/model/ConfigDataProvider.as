package application.model
{
	import flash.utils.Dictionary;

	import org.osflash.signals.Signal;
	
	import application.model.asset.BitmapItem;
	import application.model.asset.AtlasItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class ConfigDataProvider
	{

		public static const ERROR:String = 'error';
		
		///////////
		private var _atlases:Dictionary = new Dictionary();
		
		private var _fonts:Dictionary = new Dictionary();
		
		private var _bitmaps:Dictionary = new Dictionary();



		public function ConfigDataProvider() {
		}

		[PostConstruct]
		public function init():void {
			
		}

		
		private var _arrayFriends:Array;
		public function get arrayFriends():Array {
			return _arrayFriends;
		}
		
		public function get baseWidth():int {
			return 800;
		}
		
		public function get baseHeight():int {
			return 700;
		}
		
		
		//TODO сделать унификацию store, т. к. методы повторяются
		
		public function getAtlasItem(src:String, callback:Function):void {
			if (_atlases[src]) {
				_atlases[src].addCallback(callback);
			} else {
				_atlases[src] = new AtlasItem(src, callback);
			}
		}
		
		public function getBitmapItem(src:String, callback:Function):void {
			if (_bitmaps[src]) {
				_bitmaps[src].addCallback(callback);
			} else {
				_bitmaps[src] = new BitmapItem(src, callback);
			}
		}
	}

}