package application.model 
{
	/**
	 * ...
	 * @author Dee
	 */
	public class ErrorObject {
		
		private var _id:String;
		private var _msg:String;
		
		public function get id():String {
			return _id;
		}
		
		public function get msg():String {
			return _msg;
		}
		
		public function ErrorObject(id:String, msg:String) {
			_id = id;
			_msg = msg;
		}
		
	}

}