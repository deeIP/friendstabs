package application.model 
{
	
	/**
	 * ...
	 * @author Dee
	 */
	public interface ISocialProxy {
		function friendsOnline(successCallback:Function, failCallback:Function):void;
		function friendsOffline(successCallback:Function):void;
	}
	
}