package application.model.asset 
{
	
	import flash.display.Bitmap;
	
	import starling.display.Image;
	
	import utils.trigger.ITrigger;
	import utils.trigger.Trigger;
	
	
	/**
	 * ...
	 * @author Dee
	 */
	public class BitmapItem {
		
		private var _bitmap:Bitmap;
		
		private var _triggerBitmapLoaded:Trigger;
		
		private var _callbacks:Vector.<Function> = new Vector.<Function>();
		
		public function BitmapItem(src:String, callback:Function) {
			_triggerBitmapLoaded = new Trigger();
			_triggerBitmapLoaded.triggerSignal.addOnce(onAllLoaded);
			addCallback(callback);
			(new ImageLoader()).load(src, returnDataBitmap, "", false);
		}
		
		public function addCallback(callback:Function):void {
			if (_triggerBitmapLoaded.triggerValue) {
				callback(this);
			} else {
				_callbacks.push(callback);
			}
		}
		
		private function returnDataBitmap(bitmap:Bitmap, purpose:String):void {
			_bitmap = bitmap;
			_triggerBitmapLoaded.triggerValue = true;
		}
		
		private function onAllLoaded(trigger:ITrigger):void {
			for each (var callback:Function in _callbacks) {
				callback(this);
			}
		}
		
		public function get bitmap():Bitmap {
			return _bitmap;
		}
		
	}

}