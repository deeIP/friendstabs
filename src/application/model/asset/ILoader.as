package application.model.asset 
{
	
	/**
	 * ...
	 * @author Dee
	 */
	public interface ILoader {
		function load(url:String, callback:Function, param:String = '', useAddedEvent:Boolean = true):void;
	}
	
}