package application.model.asset {
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.display.Loader; 
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.system.LoaderContext;
	import flash.system.Security;
	
	import application.model.ConfigDataProvider;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class ImageLoader implements ILoader
	{
		
		private var callback:Function;
		
		private var param:String;
		
		public function ImageLoader() {
			//
		}
		private var _url:String;
		
		private var _useAddedEvent:Boolean;
		
		public function load(url:String, callback:Function, param:String = '', useAddedEvent:Boolean = true):void {
			Security.allowDomain("*");
			_url = url;
			this.callback = callback;
			this.param = param;
			_useAddedEvent = useAddedEvent;
			
			var loaderContext:LoaderContext = new LoaderContext(true);
			var loader:Loader = new Loader();
			var urlRequest:URLRequest = new URLRequest(url);
			loader.load(urlRequest, loaderContext);
			//Хак чтобы обойти отсутствие кроссдомена на vk при загрузке аватаров
			if (!_useAddedEvent) {
				loader.addEventListener(Event.ADDED, onPrepareLoader);
			} else {
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onDataLoad);
			}
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onDataIOError);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onDataSecurityError);
		}
		
		//Хак чтобы обойти отсутствие кроссдомена на vk при загрузке аватаров
		private function onPrepareLoader(e:Event):void {
			var bitmap:Bitmap = e.target as Bitmap;
			this.callback(bitmap, param);
		}
		
		private function onDataLoad(e:Event):void {
			var bitmap:Bitmap = e.target.content;
			this.callback(bitmap, param);
		}
		
		private function onDataIOError(event:Event):void {
			trace('onDataIOError');
			this.callback(null, ConfigDataProvider.ERROR);
		}
		
		private function onDataSecurityError(event:Event):void {
			trace('onDataSecurityError');
			this.callback(null, ConfigDataProvider.ERROR);
		}
		
	}
	
}