package application.model.text
{
	public class Formatter
	{
		private static const DEFAULT_PADDING_CHAR:String = " ";
		private static const EMPTY_DICT:Object = {};
		
		private static const MAIN_REGEXP:String = "%" +
			"(\\((?P<var_name>[a-zA-Z]+[_\\w]*)\\))?" +
			"(\\[(?P<lexp>!?[a-zA-Z]+[_\\w]*[^\\]]+)\\])?" +
			"((?P<arg_pos>\\d{1,2})\\$)?" +
			"((?P<padd_len>\\d{1,2})(?P<padd_align>[lLrR])?" +
			"(\\:(?<padd_char>.))?)?" +
			"(\\.(?P<precision>\\d+))?" +
			"(?P<conversion>[a-zA-Z%])";
		
		private static const LEXP_REGEXP:String = "" +
			"(?P<negative>!)?" +
			"(?P<var_name>[a-zA-Z]+[_\\w]*)" +
			"(?P<comp>[<>=]{1,2})?" +
			"(?P<eq>[\\w\\.]+)?" +
			"(\\?(?P<isPos>[^:]+))" +
			"(:(?P<isNeg>.+))?";
		
		// Converts to a string
		private static const STRING_FORMATTER:String = "s";
		// Outputs as a Number, can use the precision specifier: %.2sf will output a float with 2 decimal digits.
		private static const FLOAT_FORMATER:String = "f";
		// Outputs as an Integer.
		private static const INTEGER_FORMATER:String = "d";
		// Converts to an OCTAL number
		private static const OCTAL_FORMATER:String = "o";
		// Converts to a Hexa number (includes 0x)
		private static const HEXA_FORMATER:String = "x";
		// '%' sign
		private static const PERCENT_SIGN:String = "%";
		// '\n' sign
		private static const NEW_LINE_SIGN:String = "n";
		
		// Day of month, from 0 to 30 on <code>Date</code> objects.
		private static const DATE_DAY_FORMATTER:String = "D";
		// Full year, e.g. 2007 on <code>Date</code> objects.
		private static const DATE_FULLYEAR_FORMATTER:String = "Y";
		// Year, e.g. 07 on <code>Date</code> objects.
		private static const DATE_YEAR_FORMATTER:String = "y";
		// Month from 1 to 12 on <code>Date</code> objects.
		private static const DATE_MONTH_FORMATTER:String = "m";
		// Hours (0-23) on <code>Date</code> objects.
		private static const DATE_HOUR24_FORMATTER:String = "H";
		// Hours 0-12 on <code>Date</code> objects.
		private static const DATE_HOUR_FORMATTER:String = "I";
		// a.m or p.m on <code>Date</code> objects.
		private static const DATE_HOUR_AMPM_FORMATTER:String = "p";
		// Minutes on <code>Date</code> objects.
		private static const DATE_MINUTES_FORMATTER:String = "M";
		// Seconds on <code>Date</code> objects.
		private static const DATE_SECONDS_FORMATTER:String = "S";
		// A string rep of a <code>Date</code> object on the current locale.
		private static const DATE_TOLOCALE_FORMATTER:String = "c";
		
		
		//======================================================================
		
		public static function format(raw:String, ...args):String {
			return _format(Formatter.EMPTY_DICT, raw, args);
		}
		
		public static function format_(raw:String, ...args):String {
			return _format.apply(null, [Formatter.EMPTY_DICT, raw].concat(args));
		}
		
		public static function formatd(dict:Object, raw:String, ...args):String {
			return _format(dict, raw, args);
		}
		
		//======================================================================
		
		private static function _format(dict:Object, raw:String, args:Array):String {
			if (raw == null) {
				return "";
			}
			if (args.length == 1 && (args[0] is Array)) {
				args = args[0];
			}

			var subsParserExp:RegExp = new RegExp(Formatter.MAIN_REGEXP, "ig");
			var subs:Vector.<SubUnit> = new Vector.<SubUnit>();
			var position:int = 0;
			var result:Object;
			
			while ((result = subsParserExp.exec(raw)) != null) {
				var varName:String = result["var_name"];
				var lexp:String = result["lexp"];
				var argPos:String = result["arg_pos"];
				var paddLen:String = result["padd_len"];
				var paddAlign:String = result["padd_align"];
				var paddChar:String = result["padd_char"];
				var precision:String = result["precision"];
				var conversion:String = result["conversion"];
				var subData:String = result[0];
				var rpSource:Object;
				var rpValue:String;
				
				if (varName){
					rpSource = dict[varName];
				} else if (lexp) {
					rpSource = execLexp(lexp, dict);
				} else if (argPos) {
					var p:int = int(argPos);
					rpSource = (p >= 0 && p < args.length) ? args[p] : "";
				} else {
					rpSource = position < args.length ? args[position++] : "";
				}
				
				switch (conversion) {
					case Formatter.NEW_LINE_SIGN: {
						rpValue = "\n";
						break;
					}
					case Formatter.PERCENT_SIGN: {
						rpValue = "%";
						break;
					}
					case Formatter.STRING_FORMATTER: {
						rpValue = String(rpSource);
						break;
					}
					case Formatter.FLOAT_FORMATER: {
						var float:Number = Number(rpSource);
						rpValue = precision ? float.toFixed(int(precision)) : float.toString();
						break;
					}
					case Formatter.INTEGER_FORMATER: {
						rpValue = int(rpSource).toString();
						break;
					}
					case Formatter.OCTAL_FORMATER: {
						rpValue = "0" + int(rpSource).toString(8);
						break;
					}
					case Formatter.HEXA_FORMATER: {
						rpValue = "0x" + int(rpSource).toString(16);
						break;
					}

					// Date formatters
					case Formatter.DATE_DAY_FORMATTER: {
						rpValue = asDate(rpSource)["date"];
						break;
					}
					case Formatter.DATE_FULLYEAR_FORMATTER: {
						rpValue = asDate(rpSource)["fullYear"];
						break;
					}
					case Formatter.DATE_YEAR_FORMATTER: {
						rpValue = String(asDate(rpSource)["fullYear"]).substr(2, 2);
						break;
					}
					case Formatter.DATE_MONTH_FORMATTER: {
						rpValue = String(asDate(rpSource)["month"] + 1);
						break;
					}
					case Formatter.DATE_HOUR24_FORMATTER: {
						rpValue = asDate(rpSource)["hours"];
						break;
					}
					case Formatter.DATE_HOUR_FORMATTER: {
						var hours24:Number = asDate(rpSource)["hours"];
						rpValue = (hours24 - 12).toString();
						break;
					}
					case Formatter.DATE_HOUR_AMPM_FORMATTER: {
						rpValue = (asDate(rpSource)["hours"] >= 12 ? "p.m" : "a.m");
						break;
					}
					case Formatter.DATE_TOLOCALE_FORMATTER: {
						rpValue = (asDate(rpSource) as Date).toLocaleString();
						break;
					}
					case Formatter.DATE_MINUTES_FORMATTER: {
						rpValue = asDate(rpSource)["minutes"];
						break;
					}
					case Formatter.DATE_SECONDS_FORMATTER: {
						rpValue = asDate(rpSource)["seconds"];
						break;
					}
					default: {
						continue;
					}
				}
				
				if (paddLen != null) {
					rpValue = padString(rpValue, int(paddLen), paddChar, paddAlign);
				}
				
				var unit:SubUnit = new SubUnit();
				unit.startIndex = result.index;
				unit.endIndex = unit.startIndex + subData.length;
				unit.content = subData;
				unit.replacement = rpValue;
				subs.push(unit);
			}
			
			return buildString(raw, subs);
		}
		
		private static function asDate(source: *): Date {
			if (source is Date) {
				return source;
			}
			var result: Date = new Date();
			result.time = Number(source);
			return result;
		}
		
		//======================================================================
		
		private static function execLexp(lexp:String, dict:Object):String {
			var lexpParserExp:RegExp = new RegExp(Formatter.LEXP_REGEXP, "ig");
			var result:Object = lexpParserExp.exec(lexp);
			if (result == null) {
				return lexp;
			}
			
			var varName:String = result["var_name"];
			var comp:String = result["comp"];
			var negative:Boolean = result["negative"] ? true : false;
			var eqValueRaw:String = result["eq"];
			var isPositive:String = result["isPos"];
			var isNegative:String = result["isNeg"];
			
			if (!(comp && eqValueRaw)) {
				return (negative ? !Boolean(varValue) : Boolean(varValue)) ? isPositive : isNegative;
			}
			
			var varValue:Object = dict[varName];
			var eqValue:Object = null;
			var compResult:Boolean = false;
			
			if (varValue is Boolean) {
				if (eqValueRaw == "true") {
					eqValue = true;
				} else if (eqValueRaw == "false") {
					eqValue = false;
				} else {
					eqValue = Boolean(eqValueRaw);
				}
			} else if (varValue is String) {
				eqValue = String(eqValueRaw);
				
			} else if (varValue is int) {
				eqValue = int(eqValueRaw);
				
			} else if (varValue is Number) {
				eqValue = Number(eqValueRaw);
				
			} else {
				eqValue = eqValueRaw;
			}
			
			switch (comp) {
				case "=":  compResult = (varValue == eqValue); break;
				case ">":  compResult = (varValue > eqValue); break;
				case "<":  compResult = (varValue < eqValue); break;
				case ">=": compResult = (varValue >= eqValue); break;
				case "<=": compResult = (varValue <= eqValue); break;
				case "<>": compResult = (varValue != eqValue); break;
			}
			
			return (negative ? !compResult : compResult) ? isPositive : isNegative;
		}
		
		private static function padString(raw:String, len:int, char:String, align:String):String {
			if (len < raw.length) {
				return raw;
			}
			if (char == null || char == "") {
				char = Formatter.DEFAULT_PADDING_CHAR;
			}
			
			var padBuilder:Array = new Array(len + 1 - raw.length);
			if (align.toLowerCase() == "l") {
				return raw + padBuilder.join(char);
			}
			return padBuilder.join(char) + raw;
		}

		private static function buildString(raw:String, subs:Vector.<SubUnit>):String {
			if (subs.length == 0){
				return raw;
			}

			var buffer:Array = [];
			var lastSub:int = 0;
			
			for (var i:uint = 0, len:uint = subs.length; i < len; i++) {
				var unit:SubUnit = subs[i];
				buffer.push(raw.substring(lastSub, unit.startIndex));
				buffer.push(unit.replacement);
				lastSub = unit.endIndex;
			}
			buffer.push(raw.substr(unit.endIndex, raw.length - unit.endIndex));
			
			return buffer.join("");
		}
		
		//======================================================================
	}
}

//==============================================================================

class SubUnit
{
	public var startIndex:int;
	public var endIndex:int;
	public var content:String;
	public var replacement:String;
	
	public function SubUnit() {}
}

//==============================================================================
