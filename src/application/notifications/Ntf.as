package application.notifications
{
	import flash.utils.getQualifiedClassName;
	
	import org.osflash.signals.Signal;
		
	public class Ntf extends Signal {
		
		private var _ntfName:String = getQualifiedClassName(this);		
		
		public function Ntf(...parameters) {
			super(parameters);
		}		
		
		
		override public function dispatch(...parameters):void {
			super.dispatch.apply(this, parameters);	
		}
	}
}