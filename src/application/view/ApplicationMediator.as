package application.view {
	
	import application.view.alert.AlertView;
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import application.model.ConfigDataProvider;
	import application.view.userProfile.PickFriendsCompositionView;
	
	import application.model.VKAPIService;
	import application.model.ISocialProxy;
	import application.notifications.NtfRequestError;
	import application.model.ErrorObject;
	
	public class ApplicationMediator extends StarlingMediator {
		
		
		[Inject]
		public var configDataProvider:ConfigDataProvider;
		
		[Inject]
		public var _applicationView:ApplicationView;
		
		[Inject]
		public var ntfRequestError:NtfRequestError;
		
		override public function initialize():void {
			super.initialize();
			
			_applicationView.uiRoot.addChild(new PickFriendsCompositionView());
			
			ntfRequestError.add(onError);
		}
		
		private function onError(error:ErrorObject):void {
			if (_applicationView.uiAlert.numChildren > 0) {
				_applicationView.uiAlert.removeChildAt(0);
			}
			_applicationView.uiAlert.addChild(new AlertView(error));
		}
		
	}
}