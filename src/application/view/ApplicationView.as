package application.view {
	
	import starling.display.Sprite;
	
	public class ApplicationView extends Sprite {
		
		private var _screenRoot:Sprite;
		public function get screenRoot():Sprite {
			return _screenRoot;
		}
		
		private var _uiRoot:Sprite;
		public function get uiRoot():Sprite {
			return _uiRoot;
		}
		
		private var _uiAlert:Sprite;
		public function get uiAlert():Sprite {
			return _uiAlert;
		}
		
		public function ApplicationView() {
			super();	

			_uiRoot = new Sprite();
			addChild(_uiRoot);
			
			_uiAlert = new Sprite();
			addChild(_uiAlert);
		}
		
	}
}