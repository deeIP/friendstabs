package application.view 
{
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import application.model.ConfigDataProvider;
	import application.model.asset.AtlasItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class AtlasRequestMediator extends StarlingMediator {
		
		[Inject]
		public var atlasRequestView:IAtlasRequestView;
		
		[Inject]
		public var configDataProvider:ConfigDataProvider;
		
		override public function initialize():void {
			super.initialize();
			configDataProvider.getAtlasItem(atlasRequestView.src, sourceCallback);
		}
		
		private function sourceCallback(atlasItem:AtlasItem):void {
			atlasRequestView.setResource(atlasItem);
		}
		
	}

}