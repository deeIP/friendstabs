package application.view 
{
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import application.model.ConfigDataProvider;
	import application.model.asset.BitmapItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class BitmapRequestMediator extends StarlingMediator {
		
		[Inject]
		public var bitmapRequestView:IBitmapRequestView;
		
		[Inject]
		public var configDataProvider:ConfigDataProvider;
		
		override public function initialize():void {
			super.initialize();
			configDataProvider.getBitmapItem(bitmapRequestView.src, sourceCallback);
		}
		
		private function sourceCallback(bitmapItem:BitmapItem):void {
			bitmapRequestView.setResource(bitmapItem);
		}
		
	}

}