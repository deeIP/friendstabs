package application.view 
{
	import application.model.asset.BitmapItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public interface IBitmapRequestView {
		function setResource(resource:BitmapItem):void;
		function get src():String;
	}
	
}