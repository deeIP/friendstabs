package application.view.alert 
{
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import application.view.alert.AlertView;
	import application.notifications.NtfOverlayPanelClosed;
	/**
	 * ...
	 * @author Dee
	 */
	public class AlertMeidiator extends StarlingMediator {
		
		[Inject]
		public var alertView:AlertView;
		
		[Inject]
		public var ntfOverlayPanelClosed:NtfOverlayPanelClosed;
		
		override public function initialize():void {
			super.initialize();
			alertView.closeBtn.touchedSignal.addOnce(onClose);
		}
		
		private function onClose():void {
			ntfOverlayPanelClosed.dispatch();
			alertView.parent.removeChild(alertView);
		}
		
		override public function destroy():void {
			super.destroy();
		}
	}

}