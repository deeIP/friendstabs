package application.view.alert 
{
	import flash.geom.Rectangle;
	
	import starling.display.Sprite;
	import starling.display.Quad;
	
	import application.view.uiComponents.Panel;
	import application.view.uiComponents.Button;
	import application.view.uiComponents.TextFieldBitmapFont;
	import application.model.ErrorObject;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class AlertView extends Sprite {
		
		private var _closeBtn:Button;
		public function get closeBtn():Button {
			return _closeBtn;
		}
		
		public function AlertView(error:ErrorObject) {
			
			var bg:Quad = new Quad(800, 700, 0x000000)
			bg.alpha = 0.5;
			addChild(bg);
			
			var inputBorder:Panel = new Panel("friend_frame", null, {rectangle:new Rectangle(10, 10, 15, 15), width:400, height:300});
			addChild(inputBorder);
			inputBorder.x = 200;
			inputBorder.y = 200;
			
			
			var labelInfoTextField:Object = {
				text:"Ошибка: ",
				fontFace:"beigeKhakiOutlineArial",
				src:"https://kondrakov.github.io/beige_khaki_outline_arial",
				fontSize:20,
				color:0xffffff,
				touchable:false
			};
			var rectangleInfoTextField:Rectangle = new Rectangle(0, 0, 400, 200);
			var dataInfoTextField:Object = {src:labelInfoTextField.src, label:labelInfoTextField, fontFamily:labelInfoTextField.fontFace, area:rectangleInfoTextField};
			var titleInfoTextField:TextFieldBitmapFont =  new TextFieldBitmapFont(dataInfoTextField);
			titleInfoTextField.x = 200;
			titleInfoTextField.y = 300;
			addChild(titleInfoTextField);
			if (error.id == '15') {
				titleInfoTextField.tf.text = "Доступ запрещён";
			}
			if (error.id == '101') {
				titleInfoTextField.tf.text = "Нет доступа к API";
			}
			
			_closeBtn = new Button("WIN_btn_close");
			addChild(_closeBtn);
			_closeBtn.x = 550;
			_closeBtn.y = 210;
		}
		
	}

}