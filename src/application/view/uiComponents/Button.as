package application.view.uiComponents 
{
	import starling.display.Sprite;
	import flash.geom.Rectangle;
	import starling.display.Image;
	import starling.text.TextField;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	
	import org.osflash.signals.Signal;
	
	import application.view.IAtlasRequestView;
	import application.model.asset.AtlasItem;
	import application.view.utils.DisplayObjectParams;
	
	
	import flash.text.TextFieldType;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class Button extends Sprite implements IAtlasRequestView {
		
		private var _src:String;
		private var _id:String;
		private var _atlasItem:AtlasItem;
		
		public var touchedSignal:Signal = new Signal();
		private var _drawCompleteSignal:Signal = new Signal();
		private var _drawCompleted:Boolean;
		
		
		private var _upState:Image;
		public function get upState():Image {
			return _upState;
		}
		
		private var _overState:Image;
		public function get overState():Image {
			return _overState;
		}
		
		private var _downState:Image;
		public function get downState():Image {
			return _downState;
		}
		
		private var _disabledState:Image;
		public function get disabledState():Image {
			return _disabledState;
		}
		
		private var _disabled:Boolean;
		
		private var _tf:TextField;
		public function get tf():TextField {
			return _tf;
		}
		
		//TODO Сделать отдельным компонентом label + bitmap шрифты
		private var _label:Object;
		private var _params:DisplayObjectParams;
		
		public function Button(id:String, label:Object = null, params:DisplayObjectParams = null) {
			_id = id;
			_src = FriendsTabs.src;
			_label = label;
			_params = params;
			_drawCompleteSignal.addOnce(onDrawComleted);
		}
		
		public function get src():String {
			return _src;
		}
		
		public function get atlasItem():AtlasItem {
			return _atlasItem;
		}
		
		public function setResource(atlasItem:AtlasItem):void {
			_atlasItem = atlasItem;
			_downState = new Image(atlasItem.textureAtlas.getTexture(_id + "_norm"));
			addChild(_downState);
			
			_overState = new Image(atlasItem.textureAtlas.getTexture(_id + "_over"));
			addChild(_overState);
			
			
			_upState = new Image(atlasItem.textureAtlas.getTexture(_id + "_norm"));
			addChild(_upState);
			
			_disabledState = new Image(atlasItem.textureAtlas.getTexture(_id + "_norm"));
			_disabledState.alpha = 0.5;
			_disabledState.visible = false;
			addChild(_disabledState);
			
			if (_params) {
				_params.applyParams(_downState);
				_params.applyParams(_overState);
				_params.applyParams(_upState);
				_params.applyParams(_disabledState);
			}
			
			if (_label) {
				var rectangle:Rectangle = new Rectangle(0, 0, _upState.width, _upState.height - 5);
				var data:Object = {src:_label.src, label:_label, fontFamily:_label.fontFace, area:rectangle};
				addChild(new TextFieldBitmapFont(data));
			}
			_drawCompleteSignal.dispatch();
			addEventListener(TouchEvent.TOUCH, touchHandler);
		}


		private function touchHandler(e:TouchEvent):void {
			if (!_disabled) {
				var touch:Touch = e.getTouch(stage);
				if (touch) {
					if (touch.phase == TouchPhase.HOVER) {
						//trace('hover');
						overState.visible = true;
						upState.visible = false;
						downState.visible = false;
					}
					
					if (touch.phase == TouchPhase.BEGAN) {
						//trace('began');
						overState.visible = false;
						upState.visible = false;
						downState.visible = true;
					}
					
					if (touch.phase == TouchPhase.ENDED) {
						//trace('ended');
						touchedSignal.dispatch();
						overState.visible = true;
						upState.visible = true;
						downState.visible = true;
					}
				}
				var touchHover:Touch = e.getTouch(this, TouchPhase.HOVER);
				if (!touchHover) {
					overState.visible = false;
					upState.visible = true;
					downState.visible = false;
				}
			}
		}
		
		private function onDrawComleted():void {
			_drawCompleted = true;
			enabled(!_disabled);
		}
		
		public function enabled(value:Boolean):void {
			_disabled = !value;
			if (_drawCompleted) {
				if (!_disabled) {
					overState.visible = false;
					upState.visible = true;
					downState.visible = false;
					disabledState.visible = false;
				} else {
					overState.visible = false;
					upState.visible = false;
					downState.visible = false;
					disabledState.visible = true;
				}
			}
		}
		
	}

}