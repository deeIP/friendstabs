package application.view.uiComponents 
{
	import starling.display.Image;
	
	import application.view.utils.DisplayObjectParams;
	import application.model.asset.AtlasItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class CheckBoxStateButton extends Button {
		
		public function CheckBoxStateButton(id:String, label:Object = null, params:DisplayObjectParams = null) {
			super(id, label, params);
		}
		
		override public function setResource(atlasItem:AtlasItem):void {
			super.setResource(atlasItem);
			var check:Image = new Image(atlasItem.textureAtlas.getTexture("check"));
			addChild(check);
			check.x = (width - check.width) / 2;
		}
		
	}

}