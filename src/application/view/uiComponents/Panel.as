package application.view.uiComponents 
{
	import flash.geom.Rectangle;
	import starling.display.Sprite;
	import starling.display.Image;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.extensions.Scale9Image;
	
	import application.view.IAtlasRequestView;
	import application.model.asset.AtlasItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class Panel extends Sprite implements IAtlasRequestView {
		
		private var _src:String;
		private var _id:String;
		private var _atlasItem:AtlasItem;
		private var _bg:*;
		public function get bg():* {
			return _bg;
		}
		
		private var _label:Object;
		private var _scale9:Object;
		
		public function Panel(id:String, label:Object = null, scale9:Object = null) {
			_id = id;
			_src = FriendsTabs.src;
			_label = label;
			_scale9 = scale9;
		}
		
		public function get src():String {
			return _src;
		}
		
		public function get atlasItem():AtlasItem {
			return _atlasItem;
		}
		
		private var _tf:TextField;
		public function get tf():TextField {
			return _tf;
		}
		
		public function setResource(atlasItem:AtlasItem):void {
			_atlasItem = atlasItem;
			var mainTexture:Texture = atlasItem.textureAtlas.getTexture(_id);
			if (_scale9) {
				_bg = new Scale9Image(mainTexture, new Rectangle(_scale9.rectangle.x, _scale9.rectangle.y, _scale9.rectangle.width, _scale9.rectangle.height));
				_scale9.width >= 0 ? _bg.width = _scale9.width : null;
				_scale9.height >= 0 ? _bg.height = _scale9.height : null;
			} else {
				_bg = new Image(mainTexture);
			}
			addChild(_bg);
			
			if (_label) {
				var rectangle:Rectangle = new Rectangle(0, 0, bg.width, bg.height - 5);
				var data:Object = {src:_label.src, label:_label, fontFamily:_label.fontFace, area:rectangle};
				addChild(new TextFieldBitmapFont(data));
			}
		}
	}

}