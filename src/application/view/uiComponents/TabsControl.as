package application.view.uiComponents 
{
	import org.osflash.signals.Signal;
	
	import utils.trigger.ITrigger;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class TabsControl {
		
		private var _tabs:Vector.<ToggleButton>;
		
		private var _index:int = 0;
		
		public var selectedSignal:Signal = new Signal(int);
		
		public function TabsControl(tabs:Vector.<ToggleButton>) {
			_tabs = tabs;
			for (var i:int = 0; i < _tabs.length; i++) {
				_tabs[i].changedTrigger.triggerSignal.add(onTabTouch);
			}
			_tabs[_index].blocked(true);
			for (i = 1; i < _tabs.length; i++) {
				_tabs[i].toggleOff();
			}
		}
		
		private function onTabTouch(trigger:ITrigger):void {
			for (var i:int = 0; i < _tabs.length; i++) {
				_tabs[i].changedTrigger.triggerSignal.remove(onTabTouch);
			}
			for (i = 0; i < _tabs.length; i++) {
				_tabs[i].blocked(false);
				if (_tabs[i].changedTrigger == trigger) {
					_tabs[i].blocked(true);
					if (i != _index) {
						_index = i;
						selectedSignal.dispatch(_index);
					}
				} else {
					_tabs[i].toggleOff();
				}
			}
			for (i = 0; i < _tabs.length; i++) {
				_tabs[i].changedTrigger.triggerSignal.add(onTabTouch);
			}
		}
		
	}

}