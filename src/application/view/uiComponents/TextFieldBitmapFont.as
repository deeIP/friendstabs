package application.view.uiComponents 
{
	import flash.geom.Rectangle;
	import starling.display.Sprite;
	import starling.text.BitmapFont;
	import starling.textures.Texture;
	import starling.text.TextField;
	import starling.text.TextFormat;
	import application.view.IAtlasRequestView;
	import application.model.asset.AtlasItem;
	/**
	 * ...
	 * @author Dee
	 */
	public class TextFieldBitmapFont extends Sprite implements IAtlasRequestView {
		
		private var _src:String;
		private var _label:Object;
		private var _fontFamily:String;
		private var _area:Rectangle;
		private var _tf:TextField;
		public function get tf():TextField {
			return _tf;
		}
		//TODO улучшить передачу параметров в TextField
		public function TextFieldBitmapFont(data:Object) {
			_src = data.src;
			_label = data.label;
			_fontFamily = data.fontFamily;
			_area = data.area;
			_tf = new TextField(_area.width, _area.height, _label.text, new TextFormat("Verdana", _label.fontSize, _label.color));
			_tf.touchable = _label.touchable;
			_tf.x = _area.x;
			_tf.y = _area.y;
		}
		
		public function get src():String {
			return _src;
		}
		
		public function setResource(atlasItem:AtlasItem):void {
			var font:BitmapFont = new BitmapFont(Texture.fromBitmap(atlasItem.bitmap), atlasItem.xml);
			if (!TextField.getBitmapFont(_fontFamily)) {
				TextField.registerCompositor(font, _fontFamily);
			}
			_tf.format.font = _fontFamily;
			addChild(_tf);
		}
		
		public function setText(value:String):void {
			_tf.text = value;
		}
	}

}