package application.view.uiComponents 
{
	import utils.trigger.Trigger;
	
	import application.view.IAtlasRequestView;
	import application.model.asset.AtlasItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class ToggleButton {
		
		private var _untoggled:Button;
		private var _toggled:Button;
		
		private var _blocked:Boolean;
		
		public var changedTrigger:Trigger = new Trigger();
		
		public function ToggleButton(untoggled:Button, toggled:Button) {
			_untoggled = untoggled;
			_untoggled.touchedSignal.add(toggleOn);
			_toggled = toggled;
			_toggled.touchedSignal.add(toggleOff);
		}
		
		
		public function toggleOn():void {
			if (!_blocked) {
				_untoggled.visible = false;
				_toggled.visible = true;
				changedTrigger.triggerValue = true;
			}
		}
		
		public function toggleOff():void {
			if (!_blocked) {
				_untoggled.visible = true;
				_toggled.visible = false;
				changedTrigger.triggerValue = false;
			}
		}
		
		public function blocked(value:Boolean):void {
			_blocked = value;
		}
		
		public function get untoggled():Button {
			return _untoggled;
		}
		
		public function get toggled():Button {
			return _toggled;
		}
	}

}