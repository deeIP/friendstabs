package application.view.userProfile 
{
	import starling.events.Event;
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import feathers.events.FeathersEventType;
	
	import application.model.ConfigDataProvider;
	import application.notifications.NtfRequestError;
	import application.model.ErrorObject;
	import application.notifications.NtfOverlayPanelClosed;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class PickFriendsCompositionMediator extends StarlingMediator {
		
		[Inject]
		public var configDataProvider:ConfigDataProvider;
		
		[Inject]
		public var pickFriendsCompositionView:PickFriendsCompositionView;
		
		[Inject]
		public var ntfRequestError:NtfRequestError;
		
		[Inject]
		public var ntfOverlayPanelClosed:NtfOverlayPanelClosed;
		
		public function PickFriendsCompositionMediator() {
			super();
			
		}
		
		private var _tabIndex:int = 0;
		
		override public function initialize():void {
			super.initialize();
			pickFriendsCompositionView.x = (configDataProvider.baseWidth - 664) / 2;
			pickFriendsCompositionView.y = (configDataProvider.baseHeight - 574) / 2;
			pickFriendsCompositionView.userProfileListView.list.addEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollCompleted);
			pickFriendsCompositionView.userProfileListView.setDataChangedCallback(onChangedListData);
			pickFriendsCompositionView.userProfileListView.setErrorCallback(onError);
			pickFriendsCompositionView.bnLeft.enabled(false);
			pickFriendsCompositionView.bnRight.enabled(true);
			pickFriendsCompositionView.bnLeft.touchedSignal.add(onTouchLeft);
			pickFriendsCompositionView.bnRight.touchedSignal.add(onTouchRight);
			pickFriendsCompositionView.sendBtn.touchedSignal.add(onTouchSendSelected);
			pickFriendsCompositionView.tabsControl.selectedSignal.add(tabSelected);
			pickFriendsCompositionView.inputFilterFriends.addEventListener(Event.CHANGE, textInputHandler);
			ntfOverlayPanelClosed.add(onOverlayPanelClosed);
		}
		private function textInputHandler(event:*):void {
			pickFriendsCompositionView.userProfileListView.setSelectorText(pickFriendsCompositionView.inputFilterFriends.text);
			onChangedListData();
		}
		
		private function onScrollCompleted(e:Event):void {
			pickFriendsCompositionView.bnLeft.enabled(pickFriendsCompositionView.userProfileListView.list.horizontalPageIndex > 0);
			pickFriendsCompositionView.bnRight.enabled(pickFriendsCompositionView.userProfileListView.list.horizontalPageIndex < pickFriendsCompositionView.userProfileListView.list.maxHorizontalPageIndex);
		}
		
		private function tabSelected(index:int):void {
			_tabIndex = index;
			pickFriendsCompositionView.userProfileListView.changeDataProvider(index);
		}
		
		private function onChangedListData():void {
			pickFriendsCompositionView.bnLeft.enabled(false);
			var providerListLength:int =
				pickFriendsCompositionView.userProfileListView.list.dataProvider ?
				pickFriendsCompositionView.userProfileListView.list.dataProvider.data.length :
				0;
			pickFriendsCompositionView.bnRight.enabled(
				providerListLength > pickFriendsCompositionView.userProfileListView.dimXInd *
				pickFriendsCompositionView.userProfileListView.dimYInd
			);
		}
		
		private function onError(error:ErrorObject):void {
			pickFriendsCompositionView.inputFilterFriends.visible = false;
			ntfRequestError.dispatch(error);
		}
		
		private function onOverlayPanelClosed():void {
			pickFriendsCompositionView.inputFilterFriends.visible = true;
		}
		
		private function onTouchLeft():void {
			pickFriendsCompositionView.userProfileListView.list.scrollToPageIndex(
				pickFriendsCompositionView.userProfileListView.list.horizontalPageIndex > 0 ?
				pickFriendsCompositionView.userProfileListView.list.horizontalPageIndex - 1 :
				0,
				0
			);
			pickFriendsCompositionView.bnLeft.enabled(pickFriendsCompositionView.userProfileListView.list.horizontalPageIndex > 0);
		}
		
		private function onTouchRight():void {
			pickFriendsCompositionView.userProfileListView.list.scrollToPageIndex(
				pickFriendsCompositionView.userProfileListView.list.horizontalPageIndex < pickFriendsCompositionView.userProfileListView.list.maxHorizontalPageIndex ?
				pickFriendsCompositionView.userProfileListView.list.horizontalPageIndex + 1 :
				pickFriendsCompositionView.userProfileListView.list.maxHorizontalPageIndex,
				0
			);
			pickFriendsCompositionView.bnRight.enabled(pickFriendsCompositionView.userProfileListView.list.horizontalPageIndex > 0);
		}
		
		private function onTouchSendSelected():void {
			pickFriendsCompositionView.userProfileListView.sendToSelectedFriends(_tabIndex);
		}
		
		override public function destroy():void {
			super.destroy();
			pickFriendsCompositionView.userProfileListView.list.removeEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollCompleted);
			pickFriendsCompositionView.tabsControl.selectedSignal.remove(tabSelected);
			pickFriendsCompositionView.bnLeft.touchedSignal.remove(onTouchLeft);
			pickFriendsCompositionView.bnRight.touchedSignal.remove(onTouchRight);
			pickFriendsCompositionView.sendBtn.touchedSignal.remove(onTouchSendSelected);
			pickFriendsCompositionView.inputFilterFriends.removeEventListener(Event.CHANGE, textInputHandler);
		}
	}

}