package application.view.userProfile 
{
	import flash.filters.GlowFilter;
	import flash.events.FocusEvent;
	import flash.geom.Rectangle;
	import flash.events.Event;
	import starling.display.Sprite;
	import starling.utils.deg2rad;
	import starling.core.Starling;
	
	import application.view.uiComponents.Button;
	import application.view.uiComponents.ToggleButton;
	import application.view.uiComponents.Panel;
	import application.view.uiComponents.TabsControl;
	import application.view.utils.DisplayObjectParams;
	
	import starling.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import application.view.uiComponents.TextFieldBitmapFont;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class PickFriendsCompositionView extends Sprite {
		
		private var _fakeInputField:TextFieldBitmapFont;
		
		private var _inputFilterFriends:flash.text.TextField;
		
		public function get inputFilterFriends():flash.text.TextField {
			return _inputFilterFriends;
		}
		
		public function PickFriendsCompositionView() {
			var mainBg:Panel = new Panel("bg_big_purple");
			addChild(mainBg);
			
			var rightRat:Panel = new Panel("WIN_gift_mouseW");
			addChild(rightRat);
			rightRat.x = 460;
			rightRat.y = -20;
			
			var headerPanel:Panel = new Panel("bg_big_purple_title", 
				{text:"Выберите\n друзей", fontFace:"font", src:"https://kondrakov.github.io/font", fontSize:20, color:0xffffff, touchable:true});
			addChild(headerPanel);
			headerPanel.x = 215;
			headerPanel.y = 15;
			
			
			var tab1Dis:Button = new Button("btn_purple_dis", 
				{text:"Друзья онлайн", fontFace:"violetSimpleArial", src:"https://kondrakov.github.io/violet_simple_arial", fontSize:16, color:0x663399, touchable:true});
			var tab2Dis:Button = new Button("btn_purple_dis", 
				{text:"Друзья оффлайн", fontFace:"violetSimpleArial", src:"https://kondrakov.github.io/violet_simple_arial", fontSize:16, color:0x663399, touchable:true});
			addChild(tab1Dis);
			addChild(tab2Dis);
			tab1Dis.x = 70;
			tab1Dis.y = 147;
			tab2Dis.x = 280;
			tab2Dis.y = 147;
			
			var tabsBg:Panel = new Panel("bg_big_purple_board_gift");
			addChild(tabsBg);
			tabsBg.x = 50;
			tabsBg.y = 180;
			
			var leftRat:Panel = new Panel("WIN_gift_mouseB");
			addChild(leftRat);
			leftRat.x = -20;
			leftRat.y = 345;
			
			
			var tab1En:Button = new Button("btn_purple_en", 
				{text:"Друзья онлайн", fontFace:"violetSimpleArial", src:"https://kondrakov.github.io/violet_simple_arial", fontSize:16, color:0x663399, touchable:true});
			var tab2En:Button = new Button("btn_purple_en", 
				{text:"Друзья оффлайн", fontFace:"violetSimpleArial", src:"https://kondrakov.github.io/violet_simple_arial", fontSize:16, color:0x663399, touchable:true});
			addChild(tab1En);
			addChild(tab2En);
			tab1En.x = 70;
			tab1En.y = 147;
			tab2En.x = 280;
			tab2En.y = 147;
			
			var tabs:Vector.<ToggleButton> = new Vector.<ToggleButton>();
			tabs.push(new ToggleButton(tab1Dis, tab1En));
			tabs.push(new ToggleButton(tab2Dis, tab2En));
			_tabsControl = new TabsControl(tabs);
			
			_bnLeft = new Button("WIN_btn_arrow", null, new DisplayObjectParams([{param:"rotation", value:deg2rad(-90)}, {param:"y", value:33}]));
			addChild(_bnLeft);
			_bnLeft.x = 80;
			_bnLeft.y = 345;
			
			_bnRight = new Button("WIN_btn_arrow", null, new DisplayObjectParams([{param:"rotation", value:deg2rad(90)}]));
			addChild(_bnRight);
			_bnRight.x = 605;
			_bnRight.y = 345;
			
			_sendBtn = new Button("WIN_btn_green", 
				{text:"Отправить всем",fontFace:"whiteGreenShadowArial", src:"https://kondrakov.github.io/white_green_shadow_arial", fontSize:16, color:0xffffff, touchable:true});
			addChild(_sendBtn);
			_sendBtn.x = 260;
			_sendBtn.y = 500;

			var _closeBtn:Button = new Button("WIN_btn_close");
			addChild(_closeBtn);
			_closeBtn.x = 610;
			_closeBtn.y = 40;
			
			_userProfileListView = new UserProfileListView(2, 4);
			addChild(_userProfileListView);
			_userProfileListView.x = 135;
			_userProfileListView.y = 250;
			
			
			var inputBorder:Panel = new Panel("friend_frame", null, {rectangle:new Rectangle(10, 10, 15, 15), width:225, height:-1});
			addChild(inputBorder);
			inputBorder.x = 90;
			inputBorder.y = 200;
			
			var labelInfoTextField:Object = {
				text:"Выберите друзей, у которых хотите\n попросить жизнь",
				fontFace:"beigeKhakiOutlineArial",
				src:"https://kondrakov.github.io/beige_khaki_outline_arial",
				fontSize:20,
				color:0xffffff,
				touchable:false
			};
			var rectangleInfoTextField:Rectangle = new Rectangle(0, 0, 400, 100);
			var dataInfoTextField:Object = {src:labelInfoTextField.src, label:labelInfoTextField, fontFamily:labelInfoTextField.fontFace, area:rectangleInfoTextField};
			var titleInfoTextField:TextFieldBitmapFont =  new TextFieldBitmapFont(dataInfoTextField);
			titleInfoTextField.x = 100;
			titleInfoTextField.y = 60;
			addChild(titleInfoTextField);
			
			_inputFilterFriends = new flash.text.TextField();
			_inputFilterFriends.width = 220;
			_inputFilterFriends.height = 30;
			_inputFilterFriends.addEventListener(FocusEvent.FOCUS_IN, onFocusIn);
			_inputFilterFriends.addEventListener(FocusEvent.FOCUS_OUT, onFocusOut);
			var textFormat:TextFormat = new TextFormat("Arial", 18, 0xfcf1ca, true);
			textFormat.align = TextFormatAlign.LEFT;
			_inputFilterFriends.defaultTextFormat = textFormat;
			_inputFilterFriends.type = TextFieldType.INPUT;
			_inputFilterFriends.x = 167;
			_inputFilterFriends.y = 271;
			_inputFilterFriends.text = "Найти друга по имени";
			var glow:GlowFilter = new GlowFilter();
			glow.color = 0x62501e;
			glow.alpha = 1;
			glow.blurX  = 6;
			glow.blurY = 6;
			glow.strength = 4;
			_inputFilterFriends.filters = [glow];
			
			Starling.current.nativeOverlay.addChild(_inputFilterFriends);
		}
		
		private function onFocusIn(event:FocusEvent):void {
			_inputFilterFriends.text = "";
		}
		
		private function onFocusOut(event:FocusEvent):void {
			if (_inputFilterFriends.text == "") {
				_inputFilterFriends.text = "Найти друга по имени";
			}
		}
		
		private var _tabsControl:TabsControl;
		public function get tabsControl():TabsControl {
			return _tabsControl;
		}
		
		private var _userProfileListView:UserProfileListView;
		public function get userProfileListView():UserProfileListView {
			return _userProfileListView ;
		}
		
		private var _bnLeft:Button;
		public function get bnLeft():Button {
			return _bnLeft;
		}
		
		private var _bnRight:Button;
		public function get bnRight():Button {
			return _bnRight;
		}
		
		private var _sendBtn:Button;
		public function get sendBtn():Button {
			return _sendBtn;
		}
	}

}