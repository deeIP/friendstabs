package application.view.userProfile 
{
	//import flash.display.Bitmap;
	
	import starling.display.Sprite;
	import starling.display.Image;
	import starling.textures.Texture;
	
	import application.model.asset.BitmapItem;
	import application.view.IBitmapRequestView;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class UserAvatarView extends Sprite implements IBitmapRequestView {
		
		private var _src:String;
		
		public function get src():String {
			return _src;
		}
		
		public function UserAvatarView(src:String) {
			_src = src;
		}
		
		public function setResource(bitmapItem:BitmapItem):void {
			var avatar:Image = new Image(Texture.fromBitmap(bitmapItem.bitmap));//Image.fromBitmap(bitmapItem.bitmap);
			addChild(avatar);
			avatar.width = 40;
			avatar.height = 40;
			avatar.x = 2;
			avatar.y = 2;
		}
		
	}

}