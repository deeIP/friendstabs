package application.view.userProfile 
{
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import application.model.ISocialProxy;
	
	import application.model.ConfigDataProvider;
	
	import flash.utils.setTimeout;
	
	
	/**
	 * ...
	 * @author Dee
	 */
	public class UserProfileListMediator extends StarlingMediator {
		
		[Inject]
		public var socialProxy:ISocialProxy;
		
		[Inject]
		public var userProfileListView:UserProfileListView;
		
		[Inject]
		public var configDataProvider:ConfigDataProvider;
		
		public function UserProfileListMediator() {
			super();
		}
		
		override public function initialize():void {
			super.initialize();
			
			//TODO При таком способе обращения к API возникает Error #2044, разобраться
			//socialProxy.friendsOnline(userProfileListView.friendsOnline, userProfileListView.friendsOnlineFail);
			//setTimeout(closure, 200);
		}
		
		private function closure():void {
			//socialProxy.friendsOnline(userProfileListView.friendsOnline, userProfileListView.friendsOnlineFail);
		}
		
		override public function destroy():void {
			super.destroy();
			userProfileListView.onDestroyed();
		}
		
	}

}