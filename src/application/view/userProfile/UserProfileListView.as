package application.view.userProfile 
{
	import application.model.ErrorObject;
	import application.view.uiComponents.Panel;
	import fl.controls.ScrollPolicy;
	
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import feathers.controls.List;
	import feathers.data.ListCollection;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.layout.TiledRowsLayout;
	
	import starling.text.TextField;
	import feathers.layout.TiledRowsLayout;
	import feathers.layout.TiledColumnsLayout;
	
	import flash.utils.setTimeout;
	import vk.APIConnection;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class UserProfileListView extends Sprite {
		
		private var _list:List;
		private var _indexFriendsSelector:int = 0;
		private var _rendererList:Vector.<IListItemRenderer> = new Vector.<IListItemRenderer>();
		private var _dataChangedCallback:Function;
		private var _errorCallback:Function;
		private var _checkSelectedFriends:Array = new Array();
		
		private var _dimXInd:int;
		private var _dimYInd:int;
		public function get dimXInd():int {
			return _dimXInd;
		}
		public function get dimYInd():int {
			return _dimYInd;
		}
		
		
		public function get list():List {
			return _list;
		}
		
		private var _VK:APIConnection;
		
		private var _tempArray:Array;
		private var _tempArraySelected:Array = new Array();
		
		public function UserProfileListView(dimXInd:int, dimYInd:int) {
			super();
			_dimXInd = dimXInd;
			_dimYInd = dimYInd;
			_list = new List();
			_list.itemRendererFactory = itemRendererFactory;
			_list.horizontalScrollPolicy = ScrollPolicy.ON;
			_list.verticalScrollPolicy = ScrollPolicy.AUTO;
			_list.width = 420;
			_list.height = 250;
			_list.snapToPages = true;
			_list.elasticity = 0.1;
			
			var layout:TiledColumnsLayout = new TiledColumnsLayout();
			layout.gap = 0;
			layout.useSquareTiles = false;
			_list.layout = layout;
			
			addChild(_list);
			
			//TODO Не запрашивать данные напрямую из view, починить обращение к ISocial и убрать все запросы к api из этого класса
			//(пока это качестве обхода ошибки #2044 при запросе из ISocialProxy)
			setTimeout(closure, 700);
		}
		
		public function setSelectorText(value:String):void {
			_tempArraySelected = new Array();
			_tempArraySelected = _tempArraySelected.slice(0, 0);
			for (var i:int = 0; i < _tempArray.length; i++  ) {
				if (_tempArray[i].name.substr(0, value.length).toLowerCase() == value.toLowerCase() || value == "") {
					if (_tempArraySelected.indexOf(_tempArray[i]) == -1) {
						_tempArraySelected.push(_tempArray[i]);
					}
				}
			}
			_list.dataProvider = null;
			_list.dataProvider = new ListCollection(_tempArraySelected);
		}
		
		public function sendToSelectedFriends(tabIndex:int):void {
			_checkSelectedFriends = new Array();
			for each (var data:Object in _list.dataProvider.data) {
				if (data.selected) {
					_checkSelectedFriends.push(data.user_id);
				}
			}
			if (_checkSelectedFriends.length) {
				tabIndex == 0 ? sendRequest() : postWall();
			}
		}
		
		private function postWall():void {
			if (_checkSelectedFriends.length) {
				_VK.api('wall.post', { owner_id:_checkSelectedFriends.shift(), message: "Мои достижения", attachments:"photo5273647_456239020" }, wallPostSuccess, wallPostFail);
				setTimeout(postWall, 500);
			}
		}
		private function sendRequest():void {
			if (_checkSelectedFriends.length) {
				_VK.api('apps.sendRequest', { name:"request111", separate:1, user_id:_checkSelectedFriends.shift(), text:"Присоединяйтесь к нам!", type:"request"}, inviteSuccess, inviteFail);
				setTimeout(sendRequest, 500);
			}
		}
		
		public function setDataChangedCallback(dataChangedCallback:Function):void {
			_dataChangedCallback = dataChangedCallback;
		}
		
		public function setErrorCallback(errorCallback:Function):void {
			_errorCallback = errorCallback
		}
		
		private function closure():void {
			if (!_VK) {
				_VK = new APIConnection(FriendsTabs.flashVars);
			}
			changeDataProvider(0);
		}
		
		private function friendsGetSuccess(data:Object):void {
			var _arrayFriends:Array = new Array();
			for (var i:String in data) {
				//Друзья онлайн / оффлайн
				if (_indexFriendsSelector == 0 && data[i]["online"] == 1 || _indexFriendsSelector == 1 && data[i]["online"] == 0) {
					_arrayFriends.push({name:data[i]["first_name"], 
						thumbnail:data[i]["photo_50"],
						online:data[i]["online"],
						user_id:String(data[i]["user_id"]),
						selected:false}
					);
				}
			}
			_tempArray = _arrayFriends.slice();
			_list.dataProvider = new ListCollection(_arrayFriends);
		}
		
		private function friendsGetFail(data:Object):void {
			//tf2.appendText("Fail error_msg: "+data.error_msg+"\n");
		}
		
		private function itemRendererFactory():IListItemRenderer {
			var newRenderer:IListItemRenderer = new UserProfileView();
			(newRenderer as UserProfileView).panelTouched.add(onPanelTouched);
			(newRenderer as UserProfileView).dataChanged.add(onDataChanged);
			_rendererList = new Vector.<IListItemRenderer>();
			_rendererList.push(newRenderer);
			return newRenderer;
		}
		
		public function onDestroyed():void {
			for (var i:int = 0; i < _rendererList.length; i++) {
				(_rendererList[i] as UserProfileView).panelTouched.removeAll();
				(_rendererList[i] as UserProfileView).dataChanged.removeAll();
			}
		}
		
		private function onDataChanged(data:Object):void {
			if (data) {
				_dataChangedCallback();
			}
		}
		
		private function onPanelTouched(data:Object):void {
			if (data.online) {
				_VK.api('apps.sendRequest', { name:"request111", separate:1, user_id:data.user_id, text:"Присоединяйтесь к нам!", type:"request"}, inviteSuccess, inviteFail);
			} else {
				_VK.api('wall.post', { owner_id:data.user_id, message: "Мои достижения", attachments:"photo5273647_456239020" }, wallPostSuccess, wallPostFail);
			}
		}
		
		private function inviteSuccess(data:Object):void {
			//
		}
		
		private function inviteFail(data:Object):void {
			//tf2.text = 'error invite'+data.error_msg;
			/*var tf2:TextField = new TextField(300, 300, 'error invite '+data.error_code + " " + data.error_msg);
			tf2.border = true;
			tf2.alignPivot("center", "top");
			addChild(tf2);*/
			_errorCallback(new ErrorObject(data.error_code, data.error_msg));
		}
		
		private function wallPostSuccess(data:Object): void {
			//
		}
		private function wallPostFail(data:Object): void {
			//tf2.text = 'error post'+data.error_msg;
		}
		
		
		public function friends(array:Array):void {
			_list.dataProvider = null;
			_list.dataProvider = new ListCollection(array);
		}
		
		public function changeDataProvider(index:int):void {
			_tempArray = new Array();
			_indexFriendsSelector = index;
			
			//test:
			//_tempArray = _testArray.slice();
			//_list.dataProvider = listCollection1;
			
			//prod:
			_VK.api('friends.get', {fields:"online,photo_50"}, friendsGetSuccess, friendsGetFail);
		}
		
		
		
		/////////////////////////
		// Тестовые данные
		
		private var  _testArray:Array = [
			{ name: "ceceHolder17", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder18", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder19", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder20", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "ceaceHolder1", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder2", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder3", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder4", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder5", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder6", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder7", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder8", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder9", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder10", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder11", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder12", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder13", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder14", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder15", thumbnail: null, online:0, user_id:"01111111" },
			{ name: "1PlaceHolder16", thumbnail: null, online:0, user_id:"01111111" }
		];
		
		private function get listCollection1():ListCollection {
			return new ListCollection (
			[
				{ name: "PlaceHolder17", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder18", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder19", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder20", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder1", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder2", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder3", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder4", thumbnail: null, online:0, user_id:"01111111" },/*
				{ name: "PlaceHolder5", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder6", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder7", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder8", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder9", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder10", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder11", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder12", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder13", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder14", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder15", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "PlaceHolder16", thumbnail: null, online:0, user_id:"01111111" }*/
			]);
		}
		
		private function get listCollection2():ListCollection {
			return new ListCollection (
			[
				{ name: "ceceHolder17", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder18", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder19", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder20", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "ceaceHolder1", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder2", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder3", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder4", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder5", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder6", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder7", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder8", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder9", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder10", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder11", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder12", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder13", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder14", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder15", thumbnail: null, online:0, user_id:"01111111" },
				{ name: "1PlaceHolder16", thumbnail: null, online:0, user_id:"01111111" }
			]);
		}
		
	}

}