package application.view.userProfile 
{
	import flash.geom.Rectangle;
	import starling.display.Sprite;
	import starling.display.Image;
	import starling.text.TextField;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	import org.osflash.signals.Signal;
	
	import feathers.controls.renderers.DefaultListItemRenderer;
	
	import utils.trigger.ITrigger;
	
	import application.model.asset.AtlasItem;
	import application.view.IAtlasRequestView;
	import application.view.uiComponents.Button;
	import application.view.uiComponents.CheckBoxStateButton;
	import application.view.uiComponents.ToggleButton;
	import application.view.uiComponents.TextFieldBitmapFont;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class UserProfileView extends DefaultListItemRenderer implements IAtlasRequestView {
		
		private var _src:String;
		private var _atlasItem:AtlasItem;
		private var _textField:TextField;
		private var _name:String;
		private var _panelTouched:Signal = new Signal(Object);
		public var dataChanged:Signal = new Signal(Object);
		
		private var _avatarBg:Sprite;
		private var _userAvatarView:UserAvatarView;
		
		private var _selected:Boolean;
		
		private var checkBox:ToggleButton;
		
		public function UserProfileView() {
			super();
			width = 210;
			height = 60;
			itemHasLabel = false;
			_src = FriendsTabs.src;
			_avatarBg = new Sprite();
		}
		
		public function get src():String {
			return _src;
		}
		
		public function get atlasItem():AtlasItem {
			return _atlasItem;
		}
		
		public function setResource(atlasItem:AtlasItem):void {
			_atlasItem = atlasItem;
			var bg:Image = new Image(atlasItem.textureAtlas.getTexture("friend_board"));
			addChild(bg);
			
			var backFrameAvatar:Image = new Image(atlasItem.textureAtlas.getTexture("friend_frame"));
			backFrameAvatar.touchable = false;
			addChild(backFrameAvatar);
			addChild(_avatarBg);
			var overFrameAvatar:Image = new Image(atlasItem.textureAtlas.getTexture("friend_frame_empty"));
			overFrameAvatar.touchable = false;
			addChild(overFrameAvatar);
			
			var checkedBoxBtn:CheckBoxStateButton = new CheckBoxStateButton("checkbox");
			addChild(checkedBoxBtn);
			checkedBoxBtn.x = 160;
			checkedBoxBtn.y = 5;
			var uncheckedBoxBtn:Button = new Button("checkbox");
			addChild(uncheckedBoxBtn);
			uncheckedBoxBtn.x = 160;
			uncheckedBoxBtn.y = 5;
			
			checkBox = new ToggleButton(uncheckedBoxBtn, checkedBoxBtn as Button);
			checkBox.toggleOff();
			checkBox.changedTrigger.triggerSignal.add(onSelectedChange)
			
			var _label:Object = {
				text:"",
				fontFace:"violetSimpleArial",
				src:"https://kondrakov.github.io/violet_simple_arial",
				fontSize:16,
				color:0xffffff,
				touchable:false
			};
			var rectangle:Rectangle = new Rectangle(50, 0, 110, 40);
			var data:Object = {src:_label.src, label:_label, fontFamily:_label.fontFace, area:rectangle};
			var nameTextField:TextFieldBitmapFont = new TextFieldBitmapFont(data);
			_textField = nameTextField.tf;
			if (data) {
				_textField.text = data.name;
			}
			addChild(nameTextField);
			
			bg.addEventListener(TouchEvent.TOUCH, touchHandler);
			
		}
		
		private function touchHandler(e:TouchEvent):void {
			var touch:Touch = e.getTouch(stage);
			if (touch) {
				if (touch.phase == TouchPhase.BEGAN) {
					if (data) {
						panelTouched.dispatch(data);
					}
				}
			}
		}
		
		private function onSelectedChange(trigger:ITrigger):void {
			data.selected = trigger.triggerValue;
			_selected = trigger.triggerValue;
		}
		
		override public function get data():Object {
			return super.data;
		}
		
		override public function set data(value:Object):void {
			super.data = value;
			if (checkBox && data) {
				data.selected ? checkBox.toggleOn() : checkBox.toggleOff();
			}
			if (value) {
				if (data.thumbnail) {
					if (_userAvatarView) {
						_userAvatarView.parent.removeChild(_userAvatarView);
						_userAvatarView = null;
					}
					_userAvatarView = new UserAvatarView(data.thumbnail);
					_avatarBg.addChild(_userAvatarView);
				}
				data.selected = _selected;
				_name = value.name;
				if (_textField) {
					_textField.text = _name;
				}
			}
			dataChanged.dispatch(value);
		}
		
		public function get panelTouched():Signal {
			return _panelTouched;
		}
		
	}

}