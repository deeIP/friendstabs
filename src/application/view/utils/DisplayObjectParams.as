package application.view.utils 
{
	import starling.display.DisplayObject;
	/**
	 * ...
	 * @author Dee
	 */
	public class DisplayObjectParams {
		
		private var _params:Array;
		
		public function DisplayObjectParams(params:Array) {
			_params = params;
		}
		
		public function applyParams(displayObject:DisplayObject):void {
			for (var i:int = 0; i < _params.length; i++) {
				displayObject[_params[i].param] = _params[i].value;
			}
		}
		
	}

}