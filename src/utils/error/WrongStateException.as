package utils.error 
{
	import model.text.Formatter;
	import flash.utils.getQualifiedClassName;
	/**
	 * ...
	 * @author 
	 */
	public class WrongStateException extends Error
	{
		
		public function WrongStateException(sender:*) 
		{
			super(Formatter.format("Wrong state of %s",getQualifiedClassName(sender)));
		}
		
	}

}