package utils.extensions.actor
{
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	import org.swiftsuspenders.InjectionEvent;
	import org.swiftsuspenders.mapping.MappingEvent;
	
	import robotlegs.bender.framework.api.IContext;
	import robotlegs.bender.framework.api.IExtension;
	import robotlegs.bender.framework.api.IInjector;
	import robotlegs.bender.framework.impl.Context;

	public class ActorExtension implements IExtension {
		
		private var _servicesMap: Dictionary = new Dictionary();
		
		public function ActorExtension() {
			
		}
		
		public function extend(content: IContext): void {
			content.injector.map(ActorInjector).asSingleton();
			content.injector.addEventListener(InjectionEvent.POST_CONSTRUCT, postConstructHandler);			
			content.injector.addEventListener(MappingEvent.POST_MAPPING_REMOVE, postMappingRemoveHandler);
		}
		
		private function postConstructHandler(event: InjectionEvent): void {			
			if (event.instance is IService) {				
				(event.instance as IService).register();
				_servicesMap[event.instanceType] = event.instance; 
			}
		}
		
		private function postMappingRemoveHandler(e: MappingEvent): void {
			var instance: IService = _servicesMap[e.mappedType];
			if (instance == null) {
				return;
			}
			var injector: IInjector = e.target as IInjector;
			delete _servicesMap[e.mappedType];
			
			instance.remove();
		}
	}
}