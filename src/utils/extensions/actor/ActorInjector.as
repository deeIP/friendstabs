package utils.extensions.actor
{
	import robotlegs.bender.framework.api.IInjector;

	public class ActorInjector {
		
		[Inject] public var injector: IInjector;
		
		public function ActorInjector() {
		}
		
		public function registerActor(actor: IActor): void {
			injector.injectInto(actor);
			actor.register();
		}
		
		public function registerService(type: Class, service: IService): void {
			injector.map(type).toValue(service);
			injector.injectInto(service);			
			service.register();
		}
	}
}