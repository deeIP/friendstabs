package utils.extensions.actor
{
	/** Реализуют классы, которые инжектят внутрь себя значения, но сами не попадают в инжектор */
	public interface IActor
	{
		function register(): void;
		function remove(): void;
	}
}