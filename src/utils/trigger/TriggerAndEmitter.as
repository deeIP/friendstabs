package utils.trigger
{
	public class TriggerAndEmitter extends TriggerEmitter {
		
		public function TriggerAndEmitter(...args) {
			super(args);
		}
		
		override protected function getTriggerValue(): Boolean {
			if (_triggerList.length == 0) {
				return false;
			}
			for each (var t: ITrigger in _triggerList) {
				if (!t.triggerValue) {
					return false;
				}
			}
			return true;
		}
	}
}