package utils.trigger
{
	import flash.utils.Dictionary;
	
	import org.osflash.signals.Signal;
	
	import starling.errors.AbstractClassError;
	
	import utils.error.NotImplementsError;

	public class TriggerEmitter implements ITrigger {
		
		private var _value: Boolean = false;
		private var _triggerSignal: Signal = new Signal(ITrigger);
		protected var _triggerList: Vector.<ITrigger> = new Vector.<ITrigger>();
		
		public function TriggerEmitter(args: Array) {
			for each (var t: ITrigger in args) {
				connect(t);
			}
		}
		
		public function get numConnections(): int {
			return _triggerList.length;
		}
		
		public function isConnected(trigger: ITrigger): Boolean {
			return _triggerList.indexOf(trigger) != -1;
		}
		
		public function connect(trigger: ITrigger): void {
			if (_triggerList.indexOf(trigger) != -1) {
				return;
			}
			trigger.triggerSignal.add(triggerSignalHandler);
			_triggerList.push(trigger);
			triggerSignalHandler(this);
		}
		
		public function remove(trigger: ITrigger): void {
			var index: int = _triggerList.indexOf(trigger);
			if (index == -1) {
				return;
			}
			trigger.triggerSignal.remove(triggerSignalHandler);
			_triggerList.splice(index, 1);
			triggerSignalHandler(this);
		}
		
		public function clear(): void {
			while (_triggerList.length > 0) {
				remove(_triggerList[0]);
			}	
		}
		
		//
		
		private function triggerSignalHandler(t: ITrigger): void {
			var newValue: Boolean = getTriggerValue();
			if (_value == newValue) {
				return;
			}			
			_value = newValue;
			_triggerSignal.dispatch(this);
		}
		
		//
		
		protected function getTriggerValue(): Boolean {
			throw new NotImplementsError();
		}
		
		//
		
		public function get triggerValue(): Boolean {
			return _value;
		}
		
		public function get triggerSignal(): Signal {
			return _triggerSignal;
		}
	}
}