package utils.trigger
{
	import org.osflash.signals.Signal;
	
	public final class TriggerNot implements ITrigger
	{
		private var _trigger: ITrigger;
		
		public function TriggerNot(trigger: ITrigger)
		{
			_trigger = trigger;
		}
		
		public function get triggerValue():Boolean
		{
			return !_trigger.triggerValue;
		}
		
		public function get triggerSignal():Signal
		{
			return _trigger.triggerSignal;
		}
	}
}